from django.urls import path
from manageSales import views

urlpatterns = [
    path('putonsale/<int:id>/<str:newprice>/', views.SetSale.as_view()),
    path('removesale/<int:id>/', views.RemoveSale.as_view()),
]