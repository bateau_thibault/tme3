import requests
from rest_framework.views import APIView
from rest_framework.response import Response
from mytig.config import baseUrl
from manageSales.models import ProductSale
from manageSales.serializers import SaleInfo
from manageSales.management.updateSale import updateSale

# Create your views here.
class SetSale(APIView):

    def get_object(self, id):
        try:
            return ProductSale.objects.get(tigID=id)
        except ProductSale.DoesNotExist:
            raise Http404
    
    def get(self, request, id, newprice, format=None):
        updated=updateSale.update(id,newprice)
        print(updated)
        prod = self.get_object(id)
        serializer = SaleInfo(prod)
        response = requests.get(baseUrl+'product/'+str(serializer.data['tigID'])+'/')
        jsondata = response.json()
        #SURCHARGE DE PRODUCT/ID ET RETOUR DES DONNÉES DE LA DB LOCAL
        if(prod.sale>0.0):
            jsondata["discount"]=prod.sale
            jsondata["sale"]=True
        else:
            jsondata["discount"]=prod.sale
            jsondata["sale"]=False
        return Response(jsondata)
    
    
    
class RemoveSale(APIView):
    
    def get_object(self, id):
        try:
            return ProductSale.objects.get(tigID=id)
        except ProductSale.DoesNotExist:
            raise Http404
    
    def get(self, request, id, format=None):
        updated=updateSale.remove(id)
        prod = self.get_object(id)
        serializer = SaleInfo(prod)
        response = requests.get(baseUrl+'product/'+str(serializer.data['tigID'])+'/')
        jsondata = response.json()
        #SURCHARGE DE "PRODUCT/ID" ET RETOUR DES DONNÉES DE LA DB LOCAL
        if(prod.sale>0.0):
            jsondata["discount"]=prod.sale
            jsondata["sale"]=True
        else:
            jsondata["discount"]=prod.sale
            jsondata["sale"]=False
        return Response(jsondata)