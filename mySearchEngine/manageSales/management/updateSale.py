from django.core.management.base import BaseCommand, CommandError
from manageSales.models import ProductSale
import requests
import time

class updateSale(BaseCommand):
    
    def update(id, newprice):
        #"SIMPLE" MAJ DE LA PROMOTION SUR LE PRODUIT (ID)
        product = ProductSale.objects.get(tigID=id)
        product.sale=newprice
        product.save()
        print(product.sale)

    def remove(id):
        #ON SET LE POURCENTAGE DE PROMO A 0.0 EN BDD (LOCAL) POUR INDIQUER QU'IL N'Y A PAS DE PROMO
        product = ProductSale.objects.get(tigID=id)
        product.sale=0.0
        product.save()