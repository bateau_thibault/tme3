from django.apps import AppConfig


class ManagesalesConfig(AppConfig):
    name = 'manageSales'
