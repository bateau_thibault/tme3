from django.db import models

# Create your models here.
class ProductSale(models.Model):
    sale = models.FloatField(default=0.0)
    tigID = models.IntegerField(default='-1')
    class Meta:
        ordering = ('sale','tigID')
        #OUI JE ME SUIS TROMPÉ PENDANT LA RÉDACTION... 
        #J'AI MIS "SALE" A LA PLACE DE "DISCOUNT"..
        # . C'EST CORRIGÉ DANS LA VIEW(TRANSPARENT)