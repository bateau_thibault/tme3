from rest_framework.serializers import ModelSerializer
from manageSales.models import ProductSale

class SaleInfo(ModelSerializer):
    class Meta:
        model = ProductSale
        fields = ('sale', 'tigID')