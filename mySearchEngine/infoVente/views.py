import requests
from rest_framework.views import APIView
from rest_framework.response import Response
from mytig.config import baseUrl
from infoVente.models import AchatProduct, VentePrduct
from infoVente.serializers import AchatProdSerializer, VenteProdSerializer
from infoVente.management.updateSell import UpdateSell

from infoProduct.management.updateQty import updateQty

from manageSales.models import ProductSale
from manageSales.serializers import SaleInfo

# Create your views here.
class BuyProduct(APIView):
    def set_object(self, id,timestamp, qty, price):
        return AchatProduct.objects.create(tigID=id,date=timestamp, quantity_buy=qty, price=price)# setAchat( )

    def get(self, request, id, price, qty, timestamp, format=None):
        #On a créer l'item "achat"
        myAchat = self.set_object( id,timestamp, qty, price)
        updateQty.add(id, qty)
        serializer = AchatProdSerializer(myAchat)
        #On va rajouter le montant acheté a la quantité en stock
        
        return Response(serializer.data)
 
class SellProduct(APIView):
    def set_object(self,  id, price, qty, timestamp, sale):        
        return VentePrduct.objects.create(tigID=id,date=timestamp, quantity_sold=qty, price=price, sale=sale)

    def get(self, request, id, price, qty, timestamp, format=None):
        
        #récuperation de la promotion
        myPromo= ProductSale.objects.get(tigID=id)
        sale =SaleInfo(myPromo).data['sale']
        #Creation entrée vente
        myVente = self.set_object( id, price, qty, timestamp, sale)
        serializer = VenteProdSerializer(myVente)
        #On va enlever la qty acheté a la quantité en stock
        updateQty.remove(id, qty)
        return Response(serializer.data)


class GetCA(APIView):
    
    def getVente(self, id):
        try:
            return VentePrduct.objects.filter(tigID=id)
        except VentePrduct.DoesNotExist:
            return 0
    
    def getAchat(self, id):
        try:
            return AchatProduct.objects.filter(tigID=id)
        except AchatProduct.DoesNotExist:
            return 0
        
    def get(self, request, id):
        
        venteItems = self.getVente(id)
        achatItems = self.getAchat(id)
        
        vente=[]
        achat=[]
        gain=0
        perte=0
        for item in venteItems:
            serializer = VenteProdSerializer(item)
            if(serializer.data['sale']>0):
                gain +=((serializer.data['quantity_sold']*serializer.data['price'])*serializer.data['sale'])/100
            else:
                gain+=serializer.data['quantity_sold']*serializer.data['price']
            vente.append(serializer.data)
        for item in achatItems:
            serializer = AchatProdSerializer(item)
            perte +=serializer.data['quantity_buy']*serializer.data['price']
            achat.append(serializer.data)
        response={
            "depense":perte,
            "gain":gain,
            "ventes":vente,
            "achats":achat
        }
        print(response)
        return Response(response)
        
        