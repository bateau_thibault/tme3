from django.urls import path
from infoVente import views
from django.views.generic.base import RedirectView

urlpatterns = [
    path('buyProduct/<int:id>/<str:price>/<int:qty>/<str:timestamp>/', views.BuyProduct.as_view()),
    path('sellProduct/<int:id>/<str:price>/<int:qty>/<str:timestamp>/', views.SellProduct.as_view()),
    path('getCA/<int:id>', views.GetCA.as_view())
]