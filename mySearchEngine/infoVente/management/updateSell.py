from django.core.management.base import BaseCommand, CommandError
from infoVente.models import AchatProduct, VentePrduct
import requests
import time

class UpdateSell(BaseCommand):
    
    def setAchat(date, tigID, qty, sale):
        #"SIMPLE" MAJ DE LA PROMOTION SUR LE PRODUIT (ID)
        product = AchatProduct(date, tigID, qty, sale)
        print(product.sale)

    def setVente(date, tigID, qty, sale, price):
        #ON SET LE POURCENTAGE DE PROMO A 0.0 EN BDD (LOCAL) POUR INDIQUER QU'IL N'Y A PAS DE PROMO
        product = VentePrduct(date, tigID, qty, sale, price)
        product.save()