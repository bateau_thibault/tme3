from django.core.management.base import BaseCommand, CommandError
from infoProduct.models import ProductQuantity
import requests
import time

from manageSales.management.updateSale import updateSale

class updateQty(BaseCommand):
    
    def add(id, qty):
        #MISE A JOUR DE LA QTY
        product = ProductQuantity.objects.get(tigID=id)
        product.quantity+=qty
        product.save()
        print(product.quantity)
        #ON MET A JOUR LES PROMO A LA MISE A JOUR DE LA QTY
        #AFIN DE TOUJOUS GARDER UNE COHÉRENCE ENTRE QTY/SALE
        #if(product.quantity<=16):
        #    #SET SALE TO O.0
        #    updateSale.remove(product.tigID)
        #elif(16<product.quantity<64):
        #    #SET SALE TO 50.0
        #    updateSale.update(product.tigID, 50.0)
        #else:
        #    #setSale TO 80.0
        #    updateSale.update(product.tigID, 80.0)

        
    def remove(id, qty):
        #MISE A JOUR DE LA QTY
        product = ProductQuantity.objects.get(tigID=id)
        if(product.quantity-qty>0):
            product.quantity-=qty
        else:
            product.quantity=0
        product.save()
        #ON MET A JOUR LES PROMO A LA MISE A JOUR DE LA QTY
        #AFIN DE TOUJOUS GARDER UNE COHÉRENCE ENTRE QTY/SALE