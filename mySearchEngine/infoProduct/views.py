import requests
from rest_framework.views import APIView
from rest_framework.response import Response
from mytig.config import baseUrl
from infoProduct.models import ProductQuantity
from infoProduct.serializers import InfoProductSerializer
from infoVente.models import VentePrduct


# Create your views here.

class InfoProduct(APIView):
    
    def get_object(self, id):
        try:
            return ProductQuantity.objects.get(id=id)
        except ProductQuantity.DoesNotExist:
            raise Http404
        
    def get_qty_sold(self, id):
        try:
            qty_sold = 0
            array = VentePrduct.objects.filter(tigID=id)
            for i in range (len(array)):
                
                serializer = VenteProdSerializer(array[i])
                qty_sold +=  serializer.data["quantity_sold"]
                print(serializer.data["quantity_sold"])
            return qty_sold
        except VentePrduct.DoesNotExist:
            print("Not found")
            return 0
        
    def get(self, request, id, format=None):
        prod = self.get_object(id)
        serializer = InfoProductSerializer(prod)
        response = requests.get(baseUrl+'product/'+str(serializer.data['tigID'])+'/')
        #"SURCHARGE" DE "product/<ID>" afin de retourner la qty enregistré en BDD (local)
        jsondata = response.json()
        jsondata["quantity"] = serializer.data['quantity']
        jsondata["quantity_sold"] = self.get_qty_sold(id)
        return Response(jsondata)
    
class InfoProducts(APIView):
    def get_object(self, id):
        try:
            return ProductQuantity.objects.get(tigID=id)
        except ProductQuantity.DoesNotExist:
            raise Http404
        
    def get_qty_sold(self, id):
        try:
            return VentePrduct.objects.filter(tigID=id).count()
        except VentePrduct.DoesNotExist:
            return 0
        
    def get(self, request, format=None):
        response = requests.get(baseUrl+'products/')
        jsondata = response.json()
        jsonResponse=[]
        #"SURCHARGE" DE "products" afin de retourner la qty enregistré en BDD (local)
        for prod in jsondata:
            prodSer = self.get_object(prod['id'])
            serializer = InfoProductSerializer(prodSer)
            prod["quantity"] = serializer.data['quantity']
            prod["quantity_sold"] = self.get_qty_sold(prod['id'])
            
            jsonResponse.append(prod)
            #    
            
        
        return Response(jsonResponse)
#    def post(self, request, format=None):
#        NO DEFITION of post --> server will return "405 NOT ALLOWED"




from infoProduct.management.updateQty import updateQty
from manageSales.models import ProductSale

class IncrementQuantity(APIView):
    def get_object(self, id):
        try:
            return ProductQuantity.objects.get(tigID=id)
        except ProductQuantity.DoesNotExist:
            raise Http404
        
    def get(self, request, id, qty):
        updateQty.add(id, qty)
        prod = self.get_object(id)
        serializer = InfoProductSerializer(prod)
        response = requests.get(baseUrl+'product/'+str(serializer.data['tigID'])+'/')
        jsondata = response.json()
        jsondata["quantity"] = serializer.data['quantity']
        #ON A SET LA QTY, ON VA REMPLACER LES
        #  CHAMPS DE PRODUCTS PAR LES NOTRES
        prod = ProductSale.objects.get(tigID=id)
        print(prod)
        jsondata['discount']=prod.sale
        if(prod.sale!=0.0):
            jsondata['sale']= True
        else:
            jsondata['sale']= False
        return Response(jsondata)
        

class DecrementQuantity(APIView):
    def get_object(self, id):
        try:
            return ProductQuantity.objects.get(tigID=id)
        except ProductQuantity.DoesNotExist:
            raise Http404
        
    def get(self, request, id, qty):
        updateQty.remove(id, qty)
        prod = self.get_object(id)
        serializer = InfoProductSerializer(prod)
        response = requests.get(baseUrl+'product/'+str(serializer.data['tigID'])+'/')
        jsondata = response.json()
        jsondata["quantity"] = serializer.data['quantity']
        
        #ON A SET LA QTY, ON VA REMPLACER LES CHAMPS DE PRODUCTS PAR LES NOTRES 
        prod = ProductSale.objects.get(tigID=id)
        print(prod)
        jsondata['discount']=prod.sale
        if(prod.sale!=0.0):
            jsondata['sale']= True
        else:
            jsondata['sale']= False
            
        
        return Response(jsondata)