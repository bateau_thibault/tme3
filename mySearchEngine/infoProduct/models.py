from django.db import models
# Create your models here.

class ProductQuantity(models.Model):
    quantity = models.IntegerField(default=0)
    tigID = models.IntegerField(default='-1')
    class Meta:
        ordering = ('quantity','tigID')
